from datetime import datetime
from src.summary import Summary
from src.duration import Duration
from src.helpers import update_key_names

mappin_dict = {
    'Meeting title': 'Title',
    'Attended participants': 'Attended participants',
    'Start time': 'Start Time',
    'End time': 'End Time',
    'Meeting Start Time': 'Start Time',
    'Meeting End Time': 'End Time',
    'Meeting Id': 'Id',
    'Total Number of Participants': 'Attended participants',
    'Meeting Title': 'Title',
}


def remove_elements_from_dict(new_dict):
    if "Average attendance time" in new_dict:
        new_dict.pop('Average attendance time')
    if "Meeting duration" in new_dict:
        new_dict.pop('Meeting duration')
    if "Total Number of Participants" in new_dict:
        new_dict.pop('Total Number of Participants')


def add_id_value(new_dict, data):
    if 'Meeting title' in data:
        new_dict['Id'] = data.get('Meeting title')
    if 'Meeting Id' in data:
        new_dict['Id'] = data.get('Meeting Id')


def convert_duration_str_to_dict(new_dict):
    start_time = datetime.strptime(new_dict.get(
        'Start Time'), '%m/%d/%y, %I:%M:%S %p')
    end_time = datetime.strptime(new_dict.get(
        'End Time'), '%m/%d/%y, %I:%M:%S %p')
    duraction_dict = end_time - start_time
    hours = duraction_dict.seconds//3600
    minutes = (duraction_dict.seconds//60) % 60
    seconds = duraction_dict.seconds % 60
    new_dict['Duration'] = {
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    }


def normalize_summary(raw_data: dict):
    new_dict = {}
    for key, value in raw_data.items():
        if value.isnumeric():
            value = int(value)
        new_dict[update_key_names(key, mappin_dict)] = value
    add_id_value(new_dict, raw_data)
    convert_duration_str_to_dict(new_dict)
    remove_elements_from_dict(new_dict)
    return new_dict


def build_summary_object(raw: dict):
    duration_obj = Duration(
        raw['Duration']['hours'], raw['Duration']['minutes'], raw['Duration']['seconds'])
    summary_object = Summary(raw['Id'], raw['Title'], raw['Attended participants'],
                             raw['Start Time'], raw['End Time'], duration_obj)
    return summary_object
