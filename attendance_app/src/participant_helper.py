def get_meeting_duration(raw_participants):
    return calculate_total_in_metting_duration(raw_participants)


def handle_duration_response(total_duration):
    hours = total_duration//3600
    minutes = (total_duration//60) % 60
    seconds = total_duration % 60
    return f'{hours}h {minutes}m {seconds}s'


def get_duration_in_seconds(duration):
    seconds = int(duration.split(' ')[1].replace('s', ''))
    minutes = int(duration.split(' ')[0].replace('m', '')) * 60
    return seconds + minutes


def calculate_total_in_metting_duration(initial_raw_participants):
    total_meeting_seconds = 0
    for element in initial_raw_participants:
        total_meeting_seconds += get_duration_in_seconds(element['Duration'])
    return handle_duration_response(total_meeting_seconds)
