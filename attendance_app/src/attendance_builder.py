from src.attendance import Atendance
from src.summary import Summary
from src.duration import Duration


def build_attendance_object(raw_attendance):
    duration_obj = Duration(
        raw_attendance['Duration']['hours'],
        raw_attendance['Duration']['minutes'],
        raw_attendance['Duration']['seconds'],

    )
    sammury_obj = Summary(
        raw_attendance['Id'],
        raw_attendance['Title'],
        raw_attendance['Attended participants'],
        raw_attendance['Start Time'],
        raw_attendance['End Time'],
        duration_obj


    )
    attendance_obj = Atendance(
        raw_attendance['Start Time'],
        sammury_obj,
        raw_attendance['Participants']
    )

    return attendance_obj
