from src.duration import *


class Participant():
    def __init__(self, full_name_list, full_name, join_time, leave_time, email, role, id, in_meeting_duration):
        self.name = full_name_list[0]
        self.middle_name = full_name_list[1]
        self.first_surname = full_name_list[2]
        self.second_surname = full_name_list[3]
        self.full_name = full_name

        self.join_time = join_time
        self.leave_time = leave_time
        self.email = email
        self.role = role
        self.id = id

        self.in_meeting_duration = in_meeting_duration
