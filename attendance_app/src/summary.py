class Summary:
    def __init__(self, id, title, attended_participants, start_time, end_time, duration):
        self.id = id
        self.title = title
        self.attended_participants = attended_participants
        self.start_time = start_time
        self.end_time = end_time
        self.duration = duration
