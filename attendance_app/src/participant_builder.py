from datetime import datetime
from src.participant import Participant
from src.duration import Duration
from src.participant_helper import get_meeting_duration
from src.helpers import update_key_names


def build_dict_join_last_time(join_last_time_dict, row_participants):
    join_last_time_dict['Name'] = row_participants[0]['Full Name']
    join_last_time_dict['First Join Time'] = row_participants[0]['Join Time']
    join_last_time_dict['Last Leave time'] = row_participants[-1]['Leave Time']
    join_last_time_dict['Email'] = row_participants[0]['Email']
    join_last_time_dict['Role'] = row_participants[0]['Role']
    join_last_time_dict['Participant ID (UPN)'] = row_participants[0]['Participant ID (UPN)']


mappin_dict = {
    'Name': 'Full Name',
    'First Join Time': 'Join Time',
    'Last Leave time': 'Leave Time',
}


def convert_duration_str_to_dict(new_dict):
    start_time = datetime.strptime(new_dict.get(
        'Join Time'), date_time_format(new_dict.get('Join Time')))
    end_time = datetime.strptime(new_dict.get(
        'Leave Time'), date_time_format(new_dict.get('Leave Time')))
    duraction_dict = end_time - start_time
    new_dict['In-meeting Duration'] = {
        'Hours': duraction_dict.seconds//3600,
        'Minutes': (duraction_dict.seconds//60) % 60,
        'Seconds': duraction_dict.seconds % 60
    }


def add_new_values(key, value, normalized_dict):
    normalized_dict[key] = value


def date_time_format(date_str):
    year_format = date_str.split(',')[0].split('/')[-1]
    return '%m/%d/%y, %I:%M:%S %p' if len(year_format) == 2 else '%m/%d/%Y, %I:%M:%S %p'


def build_participant_data(full_name, email):
    full_name_email = email.split('@')[0].split('.')
    full_name_list = full_name.split(' ')
    if full_name_email[-1] == full_name_list[1]:
        full_name_list.insert(1, None)
    if full_name_list[-1] == full_name_email[-1]:
        full_name_list.insert(3, None)
    return full_name_list


def normalize_participant(raw: dict):
    normalize_dict = {}
    for key, value in raw.items():
        normalize_dict[update_key_names(key, mappin_dict)] = value
    convert_duration_str_to_dict(normalize_dict)
    add_new_values('Participant ID (UPN)',
                   raw['Email'], normalize_dict)
    if "Duration" in normalize_dict:
        normalize_dict.pop('Duration')
    return normalize_dict


def resolve_participant_join_last_time(raw: dict):
    join_last_time_dict = {}
    build_dict_join_last_time(join_last_time_dict, raw)
    metting_duration = get_meeting_duration(raw)
    join_last_time_dict['In-meeting Duration'] = metting_duration
    return join_last_time_dict


def build_participant_object(raw_obj: dict):
    if raw_obj['Full Name'] is None:
        raise ValueError('Full Name, Join time, Leave time cannot be None')

    full_name_list = build_participant_data(
        raw_obj['Full Name'], raw_obj['Email'])

    meeting_duration_obj = Duration(
        raw_obj['In-meeting Duration']['Hours'], raw_obj['In-meeting Duration']['Minutes'], raw_obj['In-meeting Duration']['Seconds'])
    participant = Participant(
        full_name_list,
        raw_obj['Full Name'],
        raw_obj['Join time'],
        raw_obj['Leave time'],
        raw_obj['Email'],
        raw_obj['Role'],
        raw_obj['Participant ID (UPN)'],
        meeting_duration_obj
    )
    return participant
